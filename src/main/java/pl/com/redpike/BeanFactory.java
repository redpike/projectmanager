package pl.com.redpike;

import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.lang.annotation.Annotation;
import java.util.Set;

/**
 * Created by Redpike.
 */
public class BeanFactory {

    private static BeanManager beanManager;

    public static<T> T get(Class<T> type, Annotation... qualifiers) {
        try {
            if(beanManager == null) {
                InitialContext initialContext = new InitialContext();
                beanManager = (BeanManager) initialContext.lookup("java:comp/BeanManager");
            }
            Set<Bean<?>> beans = beanManager.getBeans(type, qualifiers);
            if(beans.isEmpty()) {
                throw new RuntimeException("Could not locate a bean of type " + type.getName());
            }
            Bean<?> bean = beanManager.resolve(beans);
            CreationalContext<?> context = beanManager.createCreationalContext(bean);
            return (T) beanManager.getReference(bean, bean.getBeanClass(), context);
        } catch (NamingException e) {
            throw new RuntimeException("Could not get BeanManager through JNDI");
        }
    }
}
