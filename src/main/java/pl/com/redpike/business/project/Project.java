package pl.com.redpike.business.project;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Redpike.
 */

@Entity
@Table(name = "projects")
public class Project {

    @Id
    @NotNull
    @Column(name = "id_project", unique = true, precision = 10, nullable = false)
    private Long id = 0L;

    @Size(max = 30)
    @NotNull
    @Column(name = "project_name", nullable = false)
    private String projectName = "";

    @NotNull
    @Column(name = "project_status", precision = 1, nullable = false)
    private Long projectStatus = 0L;

    @NotNull
    @Column(name = "project_manager", precision = 10, nullable = false)
    private Long projectManager = 0L;

    public Project() {
    }

    public Project(Long id, String projectName, Long projectStatus, Long projectManager) {
        this.id = id;
        this.projectName = projectName;
        this.projectStatus = projectStatus;
        this.projectManager = projectManager;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public Long getProjectStatus() {
        return projectStatus;
    }

    public void setProjectStatus(Long projectStatus) {
        this.projectStatus = projectStatus;
    }

    public Long getProjectManager() {
        return projectManager;
    }

    public void setProjectManager(Long projectManager) {
        this.projectManager = projectManager;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Project project = (Project) o;

        if (id != null ? !id.equals(project.id) : project.id != null) return false;
        if (projectName != null ? !projectName.equals(project.projectName) : project.projectName != null) return false;
        if (projectStatus != null ? !projectStatus.equals(project.projectStatus) : project.projectStatus != null)
            return false;
        return projectManager != null ? projectManager.equals(project.projectManager) : project.projectManager == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (projectName != null ? projectName.hashCode() : 0);
        result = 31 * result + (projectStatus != null ? projectStatus.hashCode() : 0);
        result = 31 * result + (projectManager != null ? projectManager.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Project{" +
                "id=" + id +
                ", projectName='" + projectName + '\'' +
                ", projectStatus=" + projectStatus +
                ", projectManager=" + projectManager +
                '}';
    }
}
