package pl.com.redpike.business.team;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Redpike.
 */

@Entity
@Table(name = "teams")
public class Team {

    @Id
    @NotNull
    @Column(name = "id_team", unique = true, precision = 10, nullable = false)
    private Long id = 0L;

    @Size(max = 30)
    @NotNull
    @Column(name = "team_name", length = 30, nullable = false)
    private String teamName = "";

    @NotNull
    @Column(name = "team_leader", precision = 10, nullable = false)
    private Long teamLeader = 0L;

    public Team() {
    }

    public Team(Long id, String teamName, Long teamLeader) {
        this.id = id;
        this.teamName = teamName;
        this.teamLeader = teamLeader;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public Long getTeamLeader() {
        return teamLeader;
    }

    public void setTeamLeader(Long teamLeader) {
        this.teamLeader = teamLeader;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Team team = (Team) o;

        if (id != null ? !id.equals(team.id) : team.id != null) return false;
        if (teamName != null ? !teamName.equals(team.teamName) : team.teamName != null) return false;
        return teamLeader != null ? teamLeader.equals(team.teamLeader) : team.teamLeader == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (teamName != null ? teamName.hashCode() : 0);
        result = 31 * result + (teamLeader != null ? teamLeader.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Team{" +
                "id=" + id +
                ", teamName='" + teamName + '\'' +
                ", teamLeader=" + teamLeader +
                '}';
    }
}
