package pl.com.redpike.business.projectStatus;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Redpike.
 */

@Entity
@Table(name = "project_status")
public class ProjectStatus {

    @Id
    @NotNull
    @Column(name = "id_project_status", unique = true, precision = 10, nullable = false)
    private Long id = 0L;

    @Size(max = 20)
    @Column(name = "status_name", length = 20, nullable = false)
    private String statusName = "";

    public ProjectStatus() {
    }

    public ProjectStatus(Long id, String statusName) {
        this.id = id;
        this.statusName = statusName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProjectStatus that = (ProjectStatus) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        return statusName != null ? statusName.equals(that.statusName) : that.statusName == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (statusName != null ? statusName.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ProjectStatus{" +
                "id=" + id +
                ", statusName='" + statusName + '\'' +
                '}';
    }
}
