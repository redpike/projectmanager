package pl.com.redpike.business.user;

import pl.com.redpike.business.accStatus.AccountStatus;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Redpike.
 */

@Entity
@Table(name = "users")
public class User {

    @Id
    @NotNull
    @Column(name = "id_user", unique = true, precision = 11, nullable = false)
    private Long id = 0L;

    @Size(min = 6, max = 20)
    @NotNull
    @Column(name = "username", nullable = false)
    private String username = "";

    @Size(min = 6, max = 12)
    @NotNull
    @Column(name = "password", nullable = false)
    private String password = "";

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "status", nullable = false)
    private AccountStatus status = new AccountStatus();

    @Size(max = 255)
    @NotNull
    @Column(name = "name", nullable = false)
    private String name = "";

    @Size(max = 255)
    @NotNull
    @Column(name = "surname", nullable = false)
    private String surname = "";

    @Size(max = 255)
    @NotNull
    @Column(name = "email", nullable = false)
    private String email = "";

    @Size(max = 255)
    @NotNull
    @Column(name = "telephone", nullable = false)
    private String telephone = "";

    @Column(name = "manager", precision = 10)
    private Long manager = 0L;

    @Column(name = "team", precision = 10)
    private Long team = 0L;

    public User() {
    }

    public User(Long id, String username, String password, AccountStatus status, String name, String surname, String email, String telephone, Long manager, Long team) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.status = status;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.telephone = telephone;
        this.manager = manager;
        this.team = team;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public AccountStatus getStatus() {
        return status;
    }

    public void setStatus(AccountStatus status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public Long getManager() {
        return manager;
    }

    public void setManager(Long manager) {
        this.manager = manager;
    }

    public Long getTeam() {
        return team;
    }

    public void setTeam(Long team) {
        this.team = team;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (id != user.id) return false;
        if (status != user.status) return false;
        if (manager != user.manager) return false;
        if (team != user.team) return false;
        if (username != null ? !username.equals(user.username) : user.username != null) return false;
        if (password != null ? !password.equals(user.password) : user.password != null) return false;
        if (name != null ? !name.equals(user.name) : user.name != null) return false;
        if (surname != null ? !surname.equals(user.surname) : user.surname != null) return false;
        if (email != null ? !email.equals(user.email) : user.email != null) return false;
        return telephone != null ? telephone.equals(user.telephone) : user.telephone == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (telephone != null ? telephone.hashCode() : 0);
        result = 31 * result + (manager != null ? manager.hashCode() : 0);
        result = 31 * result + (team != null ? team.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", status=" + status +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", email='" + email + '\'' +
                ", telephone='" + telephone + '\'' +
                ", manager=" + manager +
                ", team=" + team +
                '}';
    }
}
