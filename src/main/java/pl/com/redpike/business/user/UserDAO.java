package pl.com.redpike.business.user;

import pl.com.redpike.Statics;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by Redpike.
 */
@Stateless
@LocalBean
public class UserDAO {

    @PersistenceContext(unitName = Statics.PU_NAME)
    private EntityManager em;

    public List<User> selectAll() {
        return em.createQuery("from User u order by u.username asc", User.class).setMaxResults(20).getResultList();
    }

    public User findUserById(long id) {
        return em.find(User.class, id);
    }

    public void removeUser(String username) {
        User user = em.find(User.class, username);
        em.remove(user);
    }

    public User updateUser(User user) {
        return em.merge(user);
    }

    public void createUser(User user) {
        user.setId(getNextId());
        em.persist(user);
    }

    private long getNextId() {
        return ((BigDecimal) em.createNativeQuery("Select Max(id_user) from pmg.users").getSingleResult()).longValue() + 1;
    }
}
