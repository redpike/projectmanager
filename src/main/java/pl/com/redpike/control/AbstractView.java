package pl.com.redpike.control;

import com.vaadin.cdi.CDIView;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.CustomComponent;
import org.vaadin.cdiviewmenu.ViewMenuItem;

import javax.annotation.PostConstruct;

/**
 * Created by Redpike.
 */
public abstract class AbstractView<P extends AbstractPresenter> extends CustomComponent implements ApplicationView<P>, View {

    private static final long serialVersionUID = -1501252090846963713L;

    private P presenter;

    public AbstractView() {
        setSizeFull();
    }

    @Override
    public void enter(ViewChangeEvent viewChangeEvent) {
        presenter.onViewEnter();
    }

    @PostConstruct
    protected void init() {
        setPresenter(generatePresenter());
    }

    protected abstract P generatePresenter();

    @Override
    public String getName() {
        ViewMenuItem annotation = getClass().getAnnotation(ViewMenuItem.class);
        if (annotation != null) {
            return annotation.title();
        } else {
            return getClass().getSimpleName();
        }
    }

    @Override
    public String getId() {
        CDIView annotation = getClass().getAnnotation(CDIView.class);
        if (annotation != null) {
            return annotation.value();
        }
        return super.getId();
    }

    @Override
    public P getPresenter() {
        return presenter;
    }

    protected void setPresenter(P presenter) {
        this.presenter = presenter;
        presenter.setView(this);
    }
}
