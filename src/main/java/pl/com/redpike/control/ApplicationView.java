package pl.com.redpike.control;

/**
 * Created by Redpike.
 */
public interface ApplicationView<P extends AbstractPresenter> {

    P getPresenter();

    String getName();

    String getId();
}
