package pl.com.redpike.control;

/**
 * Created by Redpike.
 */
public abstract class AbstractPresenter<V extends ApplicationView> {

    private V view;

    protected abstract void onViewEnter();

    public V getView() {
        return view;
    }

    public void setView(V view) {
        this.view = view;
    }
}
