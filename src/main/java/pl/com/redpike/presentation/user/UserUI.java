package pl.com.redpike.presentation.user;

import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.addon.jpacontainer.JPAContainerFactory;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.annotations.Widgetset;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import pl.com.redpike.business.user.User;
import pl.com.redpike.Statics;

import javax.servlet.annotation.WebServlet;

/**
 * Created by Redpike.
 */
@Theme("mytheme")
@Widgetset("pl.com.redpike.MyAppWidgetset")
public class UserUI extends UI {

    private JPAContainer<User> userJPAContainer = JPAContainerFactory.make(User.class, Statics.PU_NAME);
    private VerticalLayout verticalLayout;
    private Table userTable;

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        initComponents();
        initLayouts();
        initListeners();
    }

    private void initComponents() {
        verticalLayout = new VerticalLayout();

        userTable = new Table("User Table", userJPAContainer);
        userTable.setVisibleColumns("id", "username");
        userTable.setSelectable(true);
    }

    private void initLayouts() {
        verticalLayout.addComponent(userTable);
        verticalLayout.setMargin(true);
        verticalLayout.setSizeFull();
        verticalLayout.setComponentAlignment(userTable, Alignment.MIDDLE_CENTER);

        setContent(verticalLayout);
    }

    private void initListeners() {

    }

    @WebServlet(urlPatterns = "/*", name = "UserUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = UserUI.class, productionMode = false)
    public static class UserUIServlet extends VaadinServlet {
    }
}
